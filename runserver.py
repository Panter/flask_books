#!/home/panter/flask/bin/python
# coding: utf-8

import subprocess
import sys

from flask.ext.script import Manager

from app import app
from app.database import db_session, init_db

manager = Manager(app)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


if __name__ == "__main__":
    init_db()
    app.debug=True
    subprocess.call(['./fill_db_sqlite.sh'])
    manager.run(default_command='runserver')
