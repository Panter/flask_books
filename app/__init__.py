# coding: utf-8

import os

from flask import Flask
from flask.ext.login import LoginManager
from flask.ext.openid import OpenID
from flask_googlelogin import GoogleLogin

from config import basedir

app = Flask(__name__)
app.config.from_object('app.config')

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
lm.login_message = u'Пожалуйста, войдите, чтобы получить доступ к этой \
																	странице.'
googlelogin = GoogleLogin(app, lm)
googlelogin.GOOGLE_LOGIN_CLIENT_ID = '245192083296-ivheg2ctrvuninvh0hbp71r6rufel3no.apps.googleusercontent.com'

oid = OpenID(app, os.path.join(basedir, 'tmp'))

from app import views, models
