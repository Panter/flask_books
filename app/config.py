# coding: utf-8

import os

basedir = os.path.abspath(os.path.dirname(__file__))

SECRET_KEY = "EPj00jpfj8Gx1SjnyLxwBBSQfnQ9DJYe0Ym!"
OPENID_PROVIDERS = [
    { 'name': 'Google', 'url': 'https://accounts.google.com/o/oauth2/auth?redirect_uri=http://aqueous-harbor-2912.herokuapp.com//oauth2callback&response_type=code&client_id=245192083296-ivheg2ctrvuninvh0hbp71r6rufel3no.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile' },
    { 'name': 'Facebook', 'url': 'https://www.facebook.com/dialog/oauth?513087828812733?&redirect_uri=http://127.0.0.1:5000/' },
    { 'name': u'Вконтакте', 'url': 'http://vkontakteid.ru/' },
    { 'name': u'Яндекс', 'url': 'http://openid.yandex.ru/<username>' },
    { 'name': 'Yahoo', 'url': 'https://me.yahoo.com' },
    { 'name': 'AOL', 'url': 'http://openid.aol.com/<username>' },
    { 'name': 'Flickr', 'url': 'http://www.flickr.com/<username>' },
    { 'name': 'MyOpenID', 'url': 'https://www.myopenid.com' }]


GOOGLE_LOGIN_CLIENT_ID='245192083296-ivheg2ctrvuninvh0hbp71r6rufel3no.apps.googleusercontent.com',
GOOGLE_LOGIN_CLIENT_SECRET='-QtPo6bCMUSlH941wrLbLLeJ',
GOOGLE_LOGIN_REDIRECT_URI='http://aqueous-harbor-2912.herokuapp.com//oauth2callback',