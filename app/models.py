# coding: utf-8

from sqlalchemy import Column, Integer, String, Table, ForeignKey, \
    PrimaryKeyConstraint, SmallInteger
from sqlalchemy.orm import relationship, backref

from database import Base

books_table = Table('assoc_books_authors', Base.metadata,
                    Column('author_id', Integer, ForeignKey('authors.id')),
                    Column('book_id', Integer, ForeignKey('books.id')),
                    PrimaryKeyConstraint('author_id', 'book_id'),
                    )


class Authors(Base):
    __tablename__ = "authors"
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=False)

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return "<AUTHOR NAME: %r>" % unicode(self.name)


class Books(Base):
    __tablename__ = "books"
    id = Column(Integer, primary_key=True)
    title = Column(String(127))

    authors = relationship("Authors",
                           secondary=books_table,
                           backref=backref("books", lazy='dynamic'))

    def __init__(self, title=None):
        self.title = title

    def __repr__(self):
        return "<BOOK TITLE: %r>" % unicode(self.title)


class Users(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    username = Column(String(64))
    email = Column(String(120), index=True, unique=True)

    def __init__(self, username=None, email=None):
        self.username = username
        self.email = email

    def __repr__(self):
        return '<USER %r>' % (self.username)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonimous(self):
        return False

    def get_id(self):
        return unicode(self.id)
