# coding: utf-8

from hashlib import md5
from datetime import timedelta

from wtforms import Form, StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import InputRequired
from wtforms.ext.csrf.session import SessionSecureForm

import config

requred = u'Обязательно к заполнению'


class MyBaseForm(SessionSecureForm):
    SECRET_KEY = config.SECRET_KEY
    TIME_LIMIT = timedelta(minutes=20)


class LoginForm(MyBaseForm):
    openid = StringField('openid', [InputRequired(message=requred)])
    remember_me = BooleanField('remember_me', default=False)
    submit = SubmitField(u'Войти')


class AddBookForm(MyBaseForm):
    title = StringField(u'Название книги', 
        validators=[InputRequired(message=requred)])
    authors = StringField(u'Имя', validators=[InputRequired(message=requred)])
    submit = SubmitField(u'Сохранить')


class EditAuthor(MyBaseForm):
    name = StringField(u'Имя', validators=[InputRequired(message=requred)])
    submit = SubmitField(u'Изменить')


class EditBook(MyBaseForm):
    title = StringField(
        u'Название книги', validators=[InputRequired(message=requred)])
    submit = SubmitField(u'Изменить')


class SearchBook(Form):
    searchfield = StringField(u'Поиск',)
    submit = SubmitField(u'Поиск')
