# coding: utf-8
import json

from sqlalchemy import or_
from flask.ext.login import login_user, logout_user, current_user, \
    login_required
from flask import render_template, request, redirect, url_for, session,\
    flash, g

from forms import LoginForm, AddBookForm, EditAuthor, EditBook, SearchBook
from models import Books, Authors, Users
from app import app, lm, googlelogin, oid
from database import db_session


@lm.user_loader
def load_user(id):
    return Users.query.get(int(id))


@app.before_request
def before_request():
    g.user = current_user


@app.route('/', methods=["GET", "POST"])
@app.route('/index', methods=["GET", "POST"])
@login_required
def index():
    user = g.user
    form = SearchBook()
    if request.method == "POST" and form.validate():
        search = request.form["searchfield"].strip()
        books = Books.query.join(Books.authors).filter(
            or_(
                Books.title.like("%%%s%%" % search),
                Authors.name.like("%%%s%%" % search))
        ).all()

        return render_template('index.html', books=books, form=form,
                               path=request.url_root, searchword=search)

    books = Books.query.order_by(Books.title)
    return render_template('index.html', books=books, form=form,
                           path=request.url_root, searchword="")


@app.route('/authors')
@login_required
def authors():
    authors = Authors.query.order_by(Authors.name)
    return render_template('authors.html', authors=authors)


@app.route('/login', methods=["GET", "POST"])
@oid.loginhandler
def login():
    print "USER: ", g.user
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))

    form = LoginForm(request.form, csrf_context=session)

    if request.method == 'POST' and form.validate():
        session['remember_me'] = form.remember_me.data
        return oid.try_login(form.openid.data, ask_for=['nickname', 'email'])
    
    return render_template('login.html', 
        form=form, providers=app.config['OPENID_PROVIDERS'])


@oid.after_login
def after_login(resp):
    # print "RESPPPP: ", [i for i in dir(resp) if not i.startswith('_')], resp.email, resp.nickname, resp.aim
    if resp.email is None or resp.email == "":
        flash(u'Не верный логин или не указан email. Попробуйте еще раз.')
        return redirect(url_for('login'))
    user = Users.query.filter_by(email=resp.email).first()
    if user is None:
        nickname = resp.nickname
        if nickname is None or nickname == "":
            nickname = resp.email.split('@')[0]
        user = Users(username=nickname, email=resp.email)
        db_session.add(user)
        db_session.commit()
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
    login_user(user, remember=remember_me)
    return redirect(request.args.get('next') or url_for('index'))



@app.route('/oauth2callback')
@googlelogin.oauth2callback
def google_auth(token, userinfo, **params):
    if userinfo['email']:
        user = Users.query.filter_by(email=userinfo['email']).first()
        if not user:
            user = Users(username=userinfo['given_name'], email=userinfo['email'])
        after_login(user)
        session['token'] = json.dumps(token)
        session['extra'] = params.get('extra')
        return redirect(params.get('next', url_for('index')))
    else:
        flash(u'Произошла ошибка авторизации, попробуйте еще раз.')
        return redirect('login.html')

@app.route('/add', methods=["GET", "POST"])
@login_required
def add():
    form = AddBookForm(request.form, csrf_context=session)

    if request.method == "POST" and form.validate():
        book_exist = Books.query.filter_by(
            title=form.title.data.strip()).first()
        if book_exist:
            books_obj = book_exist
        else:
            books_obj = Books(title=form.title.data.strip())

        for author in form.authors.data.split(","):
            author_exits = Authors.query.filter_by(name=author).first()

            if author_exits:
                books_obj.authors.append(author_exits)
            else:
                books_obj.authors.append(Authors(name=author))

        db_session.add(books_obj)
        db_session.commit()
        return redirect(url_for('index'))
    return render_template('add.html', form=form)


@app.route('/edit_book/<int:id>', methods=["GET", "POST"])
@app.route('/edit_author/<int:id>', methods=["GET", "POST"])
@login_required
def edit(id=None):
    if "author" in request.path:
        edit_obj, form, tpl = (Authors(), EditAuthor, "author")
    else:
        edit_obj, form, tpl = (Books(), EditBook, "book")

    _obj = edit_obj.query.get(id)
    form = form(request.form, obj=_obj, csrf_context=session)

    if request.method == "POST" and form.validate():
        form.populate_obj(_obj)
        db_session.commit()

    return render_template('edit_' + tpl + '.html', form=form)


@app.route('/del_author/<int:id>', methods=["GET", "POST"])
@app.route('/del_book/<int:id>', methods=["GET", "POST"])
@login_required
def delete(id=None):
    edit_obj = Authors() if "author" in request.path else Books()
    row_del = edit_obj.query.get(id)
    if row_del:
        db_session.delete(row_del)
        db_session.commit()
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/about')
def about():
    return render_template('about.html')
