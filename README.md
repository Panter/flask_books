Library on Flask.

# Install sqlite3

>>sudo apt-get install sqlite3

# Clone git repository

>>mkdir somedir

>>cd somedir

>>git clone https://Panter@bitbucket.org/Panter/flask_books.git

>>cd flask_books/

# Install all modules

>>pip install -r requirements.txt

# run application

>>python runserver.py
